$(window).load(function() {
  
  // -------------------------------------
  // Un-comment to make the slider visible
  // -------------------------------------
  // $("body").removeClass('redrum');
  // $('#splash_container').hide();
  // $("#contact_form").fadeIn();
  // $("#site_footer").show();
  // $('#thank-you').fadeIn(1200);
  // -------------------------------------

var b = document.documentElement;
            b.setAttribute('data-useragent',  navigator.userAgent);
            b.setAttribute('data-platform', navigator.platform );
            b.className += ((!!('ontouchstart' in window) || !!('onmsgesturechange' in window))?' touch':'');

function iosVhHeightBug() {
        var height = $(window).height()+20;
        $("section").css('min-height', height);
    }	  

var iOS = navigator.userAgent.match(/(iPod|iPhone|iPad)/);
if(iOS){

    

    iosVhHeightBug();
    $(window).bind('resize', iosVhHeightBug);

}  



//Get Name
	$('input[name="fn"]').change(function(){
		$name = $('input[name="fn"]').val().trim();
		$('h1 #fn').text($name);
	});
	
//Get Project Name
	$('input[name="project_name"]').change(function(){
		$name = $('input[name="project_name"]').val().trim();
		$('h1 #pn').text($name);
	});
	

//Show Forms
	$("#copy_content a").click(function(){
		$("#splash_container").removeClass('running').addClass('reverse');
    $('.job-cta').fadeOut();
		
		$(this).delay(1900).queue(function(next){
			$("body").removeClass('redrum');
			next();
		});
		
		$(this).delay(1000).queue(function(next){
		    $('#splash_container').hide();
		    $("#contact_form").fadeIn();
		    $("#site_footer").show();
		    next();
		    $('input[name=fn]').focus();
		    
		});
		
		_gaq.push(['_trackEvent', 'get in touch', 'get in touch']);
		
		return false;
	});
	 
	 
	//jQuery time
	var parent, ink, d, x, y;
	$(".white_button").click(function(e){
		parent = $(this);
		//create .ink element if it doesn't exist
		if(parent.find(".ink").length === 0){
			parent.prepend("<span class='ink'></span>");
		}
			
		ink = parent.find(".ink");
		//incase of quick double clicks stop the previous animation
		ink.removeClass("animate");
		
		//set size of .ink
		if(!ink.height() && !ink.width())
		{
			//use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
			d = Math.max(parent.outerWidth(), parent.outerHeight());
			ink.css({height: d, width: d});
		}
		
		//get click coordinates
		//logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
		x = e.pageX - parent.offset().left - ink.width()/2;
		y = e.pageY - parent.offset().top - ink.height()/2;
		
		//set the position and add class .animate
		ink.css({top: y+'px', left: x+'px'}).addClass("animate");
	});
	
	
	
	//Contact Form
	var on_section = "#info";
	var counter = 1;
	var $all_fields = $(":input");
	var $info_fields = $("#info :input");
	var $project_fields = $("#project :input");
	var $budget_fields = $("#budget :input");
	var submitted_contact_form = false;
	var db_id = 0;
	
	
	
	//Continue Click
	$("#contact_form section a.continue").click(function() {
		
		var google_section = on_section.replace('#','');
		_gaq.push(['_trackEvent', 'get in touch', google_section]);
	    
	    //Show Section
	    on_section = "#"+$(this).data('next');
	    $(on_section).fadeIn(1200);
	    
	    //Hide section continue button
	    $(this).removeClass('fade_valid');
	    
	    //Scroll to section
	    $('html,body').animate({
        	scrollTop: $(on_section).offset().top
        }, 1000);
	    
	    //Set Counter
	    counter++;
	    $('#counter span').html(counter+'/3');
	    
		//Focus on project name
	    if(on_section === '#project' && $(window).width() > 768){
	    	$('input[name=project_name]').focus();
      	}
	    	
	    return false; 
	});
	
	
	//Check for empty on info fields
    $info_fields.keyup(function() {
    
        var $emptyFields = $info_fields.filter(function() {
            return $.trim(this.value) === "";
        });
        
        var email_check = false;
		if(validateEmail($('input[name=email]').val())){
			email_check = true;
		}

        if (!$emptyFields.length && email_check === true)
        {
           $(on_section+' a.continue').addClass('fade_valid');
           
           //Save to DB
           var post_data = {
	           'step'	: 'info',
	           'db_id'	: db_id,
	           'name'	: $('input[name=fn]').val(), 
	           'email'  : $('input[name=email]').val(),
	           'Pricesuit'       	: $('input[name=Pricesuit]').val()
	        };
	        
	        $.post('contact.php', post_data, function(response)
	        {  
	            if(response.type === 'success'){
	            	db_id = response.db_id;
	            }
	            	
	        }, 'json'); 
        } 
    });
    
    //Check for empty on project fields
    $project_fields.keyup(function() {
    
        var $emptyFields = $project_fields.filter(function() {
            return $.trim(this.value) === "";
        });

        if (!$emptyFields.length)
        {
           $(on_section+' a.continue').addClass('fade_valid');
           
           if(db_id !== 0)
           {
	          	//Save to DB
			  	var post_data = {
		           'step'	: 'project',
		           'db_id'	: db_id,
		           'project_name'  : $('input[name=project_name]').val(), 
				   'project_desc'  : $('textarea[name=project_description]').val(), 
				   'Pricesuit'       	: $('input[name=Pricesuit]').val()
		        };
		        
		        $.post('contact.php', post_data, function(response){}, 'json');
		    }
        } 
    });
    
    $budget_fields.click(function() {
        $(on_section+' a.submit_form').addClass('fade_valid'); 
    });
    
    
    $('a.submit_form').click(function()
    {
	    _gaq.push(['_trackEvent', 'get in touch', 'form submitted']);
	    
        var post_data = {
	        'step'		: 'final',
            'db_id'  	: db_id, 
            'budget'	: $('input[name=budget]:checked').val(),
            'Pricesuit'    : $('input[name=Pricesuit]').val()
        };
        
        $.post('contact.php', post_data, function(response)
        {  
            if(response.type === 'success')
            {     
            	$('#thank-you').fadeIn(1200);
	    
			    //Scroll to section
			    $('html,body').animate({
		        	scrollTop: $('#thank-you').offset().top
		        }, 1000);  	
		        
		        $('#counter').addClass('success'); 
		        $('.carousel-content').slick('setPosition');
            }
            else
            {
	            alert(response.message);
            }
        }, 'json'); 
        
        submitted_contact_form = true;
        $(this).removeClass('fade_valid');
                
        return false;
    });
    
    //all fields check after submit
    $all_fields.keyup(function() {
    	
    	if(submitted_contact_form === true)
    	{
    		$('a.submit_form').addClass('fade_valid'); 
    		submitted_contact_form = false;
    	} 
    	
    });
	
	
	function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	} 
	
});


var trackOutboundLink = function(url)
{
	_gaq.push(['_trackEvent', 'footer links', url]);
	if(url === 'mailto:connect@Pricesuit.ca'){
		document.location = url;
	}
	else {
		window.open(url, '_blank');
	}
};

$(document).ready(function(){
	$('.carousel-content').slick({


		centerMode: true,
		centerPadding: '0px',
		slidesToShow: 3,
		focusOnSelect: true,
		arrows: false,
    swipeToSlide: true,
  	responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          centerPadding: '20%',
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          centerPadding: '20%',
        }
      },
    ]
	});
  
});